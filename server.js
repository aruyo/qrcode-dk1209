const http = require('http');
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser')
const app = express();
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.json());
app.use(express.static("express"));
var urlencodedParser = bodyParser.urlencoded({ extended: true })
var QRcode = require('qrcode');
const { url } = require('inspector');
// default URL for website
app.get('/', function (req, res) {
    res.render('index');
});

app.post('/', urlencodedParser, function (req, res) {
    console.log(req.body)
    const itemName = req.body.item_name
    const sku = req.body.sku
    const size = req.body.size
    const qc = QRcode.toDataURL(itemName, [])
    var opts = {
        errorCorrectionLevel: 'H',
        type: 'image/jpeg',
        quality: 0.3,
        margin: 1,
        width: parseInt(size),
        color: {
            dark: "#000000",
            light: "#FFFFFF"
        }
    }
    QRcode.toDataURL(itemName, opts, function (err, url) {
        if (err) throw res.send("error occured")

        res.render('qrcode', { "url": url, "data":req.body });
    })
})

app.get('/qrcode', function (req, res) {
    res.render('qrcode')
})

const server = http.createServer(app);
const port = 3000;
server.listen(port);
console.debug('Server listening on port ' + port);